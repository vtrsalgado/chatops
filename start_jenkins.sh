#!/bin/bash

mkdir -p jenkins
docker rm -f jenkins_devopynho || true
docker run -d \
--name jenkins_devopynho \
-p 8080:8080 \
-p 50000:50000 \
-v "$(pwd)/jenkins":/var/jenkins_home \
jenkins:latest
