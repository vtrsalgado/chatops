from slackclient import SlackClient
import jenkins
import time, os

server = jenkins.Jenkins(
    'http://localhost:8080', username='admin', password='admin')


def jobs():
    return server.get_all_jobs()


def slaves():
    return server.get_nodes()


def construir():
    server.build_job('sample_nodejs_website')
    return server.get_queue_info()


def console():
    last_build = server.get_job_info(
        'sample_nodejs_website')['lastSuccessfulBuild']['number']
    return server.get_build_console_output('sample_nodejs_website', last_build)


BOT_ID = os.environ.get("BOT_ID")
slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))
slack_client.rtm_connect()
print("Conectado à API do Slack com sucesso.")

while True:
    for message in slack_client.rtm_read():
        if message['type'] == 'message' and message['user'] != BOT_ID:
            text = message['text']
            channel = message['channel']
            print(text)

            if text == "jobs":
                response = "```%s```" % jobs()
            elif text == "slaves":
                response = "```%s```" % slaves()
            elif text == "construir":
                response = "```%s```" % construir()
            elif text == "console":
                response = "```%s```" % console()
            else:
                response = "Oi Amiguinho! Dêxa di sêr bexta, use os comandos:\n jobs, slaves, construir ou console. Seu palmitão!!!"

            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=response,
                mrkdwn=True,
                as_user=True)
    time.sleep(2)
