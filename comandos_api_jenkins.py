import jenkins
server = jenkins.Jenkins(
    'http://localhost:8080', username='admin', password='admin')

# Ver todos os jobs
server.get_all_jobs()

# Ver os nós
server.get_nodes()

# Ver a saída do ultimo build do job
last_build = server.get_job_info(
    'sample_nodejs_website')['lastSuccessfulBuild']['number']
server.get_build_console_output('sample_nodejs_website', last_build)

# Executar job
server.build_job('sample_nodejs_website')

# Fila
server.get_queue_info()
