from slackclient import SlackClient
import time, os

BOT_ID = os.environ.get("BOT_ID")
slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))
slack_client.rtm_connect()
print("Conectado à API do Slack com sucesso.")
while True:
    for message in slack_client.rtm_read():
        if message['type'] == 'message':
            text = message['text']
            channel = message['channel']
            print(text)
    time.sleep(2)
